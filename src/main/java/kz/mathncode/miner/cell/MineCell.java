package kz.mathncode.miner.cell;

import kz.mathncode.miner.exception.InvalidClickException;
import kz.mathncode.miner.exception.MineClickException;

public class MineCell extends Cell {

    public MineCell(Position position, boolean visible) {
        super(position, visible);
    }

    @Override
    public void onClick() throws InvalidClickException {
        this.setVisible(true);
        throw new MineClickException(this);
    }

    @Override
    public String symbol() {
        if (isVisible()) {
            return "\uD83D\uDCA3";
        } else {
            return "\u2B1B";
        }
    }

    @Override
    public String toString() {
        return "MineCell{} " + super.toString();
    }
}
