package kz.mathncode.miner.cell;

import kz.mathncode.miner.exception.InvalidClickException;

import java.util.Objects;

public abstract class Cell {
    private Position position;
    private boolean visible;

    public Cell(Position position, boolean visible) {
        this.position = position;
        this.visible = visible;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public abstract void onClick() throws InvalidClickException;
    public abstract String symbol();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return visible == cell.visible && Objects.equals(position, cell.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, visible);
    }

    @Override
    public String toString() {
        return "Cell{" +
                "position=" + position +
                ", visible=" + visible +
                '}';
    }
}
