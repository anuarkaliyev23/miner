package kz.mathncode.miner.cell;

import kz.mathncode.miner.exception.InvalidClickException;

import java.util.Objects;

public class NumberCell extends Cell {
    private int minesNearby;

    public NumberCell(Position position, boolean visible, int minesNearby) {
        super(position, visible);
        this.minesNearby = minesNearby;
    }

    public int getMinesNearby() {
        return minesNearby;
    }

    public void setMinesNearby(int minesNearby) {
        this.minesNearby = minesNearby;
    }

    @Override
    public void onClick() throws InvalidClickException {
        this.setVisible(true);
    }

    @Override
    public String symbol() {
        if (isVisible()) {
            switch (minesNearby) {
                case 1: return "\uff11";
                case 2: return "\uff12";
                case 3: return "\uff13";
                case 4: return "\uff14";
                case 5: return "\uff15";
                case 6: return "\uff16";
                case 7: return "\uff17";
                case 8: return "\uff18";
                case 9: return "\uff19";
                default: throw new RuntimeException(String.format("No symbol for {%s} mines nearby", minesNearby));
            }
        } else {
            return "\u2B1B";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NumberCell that = (NumberCell) o;
        return minesNearby == that.minesNearby;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), minesNearby);
    }

    @Override
    public String toString() {
        return "NumberCell{" +
                "mines=" + minesNearby +
                "} " + super.toString();
    }
}
