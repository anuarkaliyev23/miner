package kz.mathncode.miner.cell;

import kz.mathncode.miner.exception.InvalidClickException;

public class BlankCell extends Cell {

    public BlankCell(Position position, boolean visible) {
        super(position, visible);
    }

    @Override
    public void onClick() {
        if (isVisible()) {
            throw new InvalidClickException(this);
        } else {
            this.setVisible(true);
        }
    }

    @Override
    public String symbol() {
        if (isVisible()) {
            return "\u2B1C";
        } else {
            return "\u2B1B";
        }
    }

    @Override
    public String toString() {
        return "BlankCell{} " + super.toString();
    }
}
