package kz.mathncode.miner;

import kz.mathncode.miner.cell.Position;
import kz.mathncode.miner.exception.InvalidClickException;
import kz.mathncode.miner.exception.MineClickException;
import kz.mathncode.miner.factory.GameFactory;
import kz.mathncode.miner.factory.MineCellGameFactory;
import kz.mathncode.miner.game.Game;
import kz.mathncode.miner.ui.ConsoleUI;
import kz.mathncode.miner.ui.UI;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Mines count:");
        int mines = scanner.nextInt();

        System.out.println("Width: ");
        int width = scanner.nextInt();

        System.out.println("Height: ");
        int height = scanner.nextInt();

        GameFactory factory = new MineCellGameFactory(mines, width, height);
        Game game = factory.produce();
        UI gameUI = new ConsoleUI(game, scanner);

        while (true) {
            gameUI.show();
            try {
                Position position = gameUI.nextClick();
                gameUI.getGame().cell(position).onClick();
            } catch (MineClickException mce) {
                gameUI.show();
                System.out.println("You clicked on mine, GAME OVER");
                break;
            } catch (InvalidClickException ice) {
                System.out.println("Invalid click! try again");
                continue;
            } catch (Exception e) {
                System.out.println("Unknown error. Exiting...");
                System.exit(1);
            }
        }
    }
}
