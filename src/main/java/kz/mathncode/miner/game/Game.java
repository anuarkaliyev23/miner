package kz.mathncode.miner.game;

import kz.mathncode.miner.cell.Cell;
import kz.mathncode.miner.cell.Position;
import kz.mathncode.miner.exception.CellNotFoundException;

import java.util.List;
import java.util.Objects;

public class Game {
    private List<Cell> cells;

    public Game(List<Cell> cells) {
        this.cells = cells;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    public int width() {
        int maxWidth = 0;
        for (Cell cell : cells) {
            if (cell.getPosition().getX() > maxWidth) {
                maxWidth = cell.getPosition().getX();
            }
        }
        return maxWidth + 1;
    }

    public int height() {
        int maxHeight = 0;
        for (Cell cell : cells) {
            if (cell.getPosition().getY() > maxHeight) {
                maxHeight = cell.getPosition().getY();
            }
        }
        return maxHeight + 1;
    }

    public void handleClick(Position position) {
        Cell cell = cell(position);
        cell.onClick();
    }

    public Cell cell(Position position) {
        for (Cell cell : cells) {
            if (cell.getPosition().equals(position))
                return cell;
        }
        throw new CellNotFoundException(position);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(cells, game.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cells);
    }

    @Override
    public String toString() {
        return "Game{" +
                "cells=" + cells +
                '}';
    }
}
