package kz.mathncode.miner.ui;

import kz.mathncode.miner.cell.Position;
import kz.mathncode.miner.game.Game;

public interface UI {

    Game getGame();
    void show();
    Position nextClick();
}
