package kz.mathncode.miner.ui;

import kz.mathncode.miner.cell.Cell;
import kz.mathncode.miner.cell.Position;
import kz.mathncode.miner.game.Game;

import java.util.Scanner;

public class ConsoleUI implements UI {
    private final Game game;
    private final Scanner scanner;

    public ConsoleUI(Game game, Scanner scanner) {
        this.game = game;
        this.scanner = scanner;
    }

    @Override
    public Game getGame() {
        return game;
    }

    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public void show() {
        for (int i = 0; i < game.width(); i++) {
            for (int j = 0; j < game.height(); j++) {
                Position position = new Position(i, j);
                Cell cell = game.cell(position);
                System.out.print(cell.symbol());
            }
            System.out.println();
        }
    }

    @Override
    public Position nextClick() {
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        return new Position(x, y);
    }
}
