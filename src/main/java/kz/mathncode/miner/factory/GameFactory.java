package kz.mathncode.miner.factory;

import kz.mathncode.miner.game.Game;

public interface GameFactory {
    Game produce();
}
