package kz.mathncode.miner.factory;

import kz.mathncode.miner.cell.*;
import kz.mathncode.miner.exception.GameNotInstantiable;
import kz.mathncode.miner.game.Game;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class MineCellGameFactory implements GameFactory {
    private final int minesCount;
    private final int width;
    private final int height;

    public  MineCellGameFactory(int minesCount, int width, int height) {
        checkMinesCount(minesCount, width, height);
        this.minesCount = minesCount;
        this.width = width;
        this.height = height;
    }

    private void checkMinesCount(int minesCount, int width, int height) {
        if (minesCount > width * height)
            throw new GameNotInstantiable();
    }

    public int getMinesCount() {
        return minesCount;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Game produce() {
        List<Cell> mineCells = randomMineCells(minesCount);
        List<NumberCell> numberCells = numberCells(mineCells);

        List<Cell> mergedCells = new ArrayList<>();
        mergedCells.addAll(mineCells);
        mergedCells.addAll(numberCells);
        List<BlankCell> blankCells = blankCells(mergedCells);
        mergedCells.addAll(blankCells);

        return new Game(mergedCells);
    }

    private List<Cell> randomMineCells(int minesCount) {
        List<Cell> cells = new ArrayList<>();
        int counter = 0;
        while(counter < minesCount) {
            MineCell mineCell = randomMineCell();
            if (findCellByPosition(mineCell.getPosition(), cells) == null) {
                counter++;
                cells.add(mineCell);
            }
        }
        System.out.printf("Generated {%s} MineCells\n", cells);
        return cells;
    }

    private List<NumberCell> numberCells(List<Cell> mineCells) {
        Map<Position, Integer> mineCounts = new HashMap<>();
        for (Cell cell: mineCells) {
            Set<Position> adjacentPositions = adjacentPositions(cell.getPosition());
            for (Position position : adjacentPositions) {
                if (findCellByPosition(position, mineCells) != null)
                    continue;
                if (mineCounts.get(position) == null) {
                    mineCounts.put(position, 1);
                } else {
                    mineCounts.put(position, mineCounts.get(position) + 1);
                }
            }
        }

        List<NumberCell> cells = new ArrayList<>();
        for (Position position : mineCounts.keySet()) {
            NumberCell cell = new NumberCell(position, false, mineCounts.get(position));
            System.out.printf("Adding NumberCell {%s} based on mine counts\n", cell);
            cells.add(cell);
        }
        System.out.printf("Generated {%s} NumberCells\n", cells.size());
        return cells;
    }

    private List<BlankCell> blankCells(List<Cell> filledCells) {
        List<BlankCell> cells = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Position position = new Position(i, j);
                Cell cell = findCellByPosition(position, filledCells);
                System.out.printf("Inspecting position {%s} to produce a BlankCell\n", position);
                if (cell == null) {
                    BlankCell blankCell = new BlankCell(position, false);
                    System.out.printf("Produced BlankCell {%s}\n", blankCell);
                    cells.add(blankCell);
                }
            }
        }

        System.out.printf("Produced {%s} BlankCells", cells);
        return cells;
    }

    private Cell findCellByPosition(Position position, List<Cell> cells) {
        for (Cell cell : cells) {
            if (cell.getPosition().equals(position)) {
                System.out.printf("Found cell {%s} by position {%s}\n", cell, position);
                return cell;
            }
        }
        System.out.printf("Found no cell at position {%s}\n", position);
        return null;
    }

    private MineCell randomMineCell() {
        int x = ThreadLocalRandom.current().nextInt(0, width);
        int y = ThreadLocalRandom.current().nextInt(0, height);
        Position position = new Position(x, y);
        MineCell cell = new MineCell(position, false);
        System.out.printf("Generated MineCell {%s}\n", cell);
        return cell;
    }

    private Set<Position> adjacentPositions(Position position) {
        Set<Position> positions = new HashSet<>();
        for (int i = position.getX() - 1; i <= position.getX() + 1; i++) {
            for (int j = position.getY() - 1; j <= position.getY() + 1; j++) {
                if ((i >= 0  && j >= 0) && (i < width && j < height))
                    positions.add(new Position(i, j));
            }
        }
        positions.remove(position);
        System.out.printf("Found {%s} adjacent positions to position {%s}\n", positions, position);
        return positions;
    }


}
