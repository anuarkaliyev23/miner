package kz.mathncode.miner.exception;

import kz.mathncode.miner.cell.Cell;

public class MineClickException extends RuntimeException {
    private final Cell cell;

    public MineClickException(Cell cell) {
        super(String.format("Cell {%s} was a mine", cell));
        this.cell = cell;
    }

    public MineClickException(Throwable cause, Cell cell) {
        super(String.format("Cell {%s} was a mine", cell),cause);
        this.cell = cell;
    }
}
