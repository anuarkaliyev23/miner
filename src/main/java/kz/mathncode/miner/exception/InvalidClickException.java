package kz.mathncode.miner.exception;

import kz.mathncode.miner.cell.Cell;

public class InvalidClickException extends RuntimeException {
    private final Cell cell;

    public InvalidClickException(Cell cell) {
        super(String.format("Cell {%s} cannot be clicked", cell));
        this.cell = cell;
    }

    public InvalidClickException(Throwable cause, Cell cell) {
        super(String.format("Cell {%s} cannot be clicked", cell),cause);
        this.cell = cell;
    }
}
