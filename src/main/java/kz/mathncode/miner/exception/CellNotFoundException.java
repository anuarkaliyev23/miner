package kz.mathncode.miner.exception;

import kz.mathncode.miner.cell.Position;

public class CellNotFoundException extends RuntimeException {
    private final Position position;

    public CellNotFoundException(Position position) {
        super(String.format("Cell was not found at {%s}", position));
        this.position = position;
    }

    public CellNotFoundException(Throwable cause, Position position) {
        super(String.format("Cell was not found at {%s}", position),cause);
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }
}
