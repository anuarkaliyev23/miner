package kz.mathncode.miner.ui;

import kz.mathncode.miner.cell.Cell;
import kz.mathncode.miner.factory.MineCellGameFactory;
import kz.mathncode.miner.game.Game;
import org.junit.jupiter.api.Test;
import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ConsoleUITest {

    @Test
    void testShowAllNotVisible() {
        Game game = new MineCellGameFactory(5, 5, 5).produce();
        UI ui = new ConsoleUI(game, new Scanner(System.in));
        ui.show();
    }

    @Test
    void testShowALlVisible() {
        Game game = new MineCellGameFactory(5, 5, 5).produce();
        UI ui = new ConsoleUI(game, new Scanner(System.in));
        for (Cell cell : game.getCells()) {
            cell.setVisible(true);
        }
        ui.show();
    }

}