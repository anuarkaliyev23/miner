package kz.mathncode.miner.factory;

import kz.mathncode.miner.cell.MineCell;
import kz.mathncode.miner.factory.GameFactory;
import kz.mathncode.miner.factory.MineCellGameFactory;
import kz.mathncode.miner.game.Game;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MineCellGameFactoryTest {
    private static Stream<Arguments> provideArgumentsForProduce() {
        return Stream.of(
                Arguments.of(9,1,15),
                Arguments.of(3,5,1),
                Arguments.of(5,4,5),
                Arguments.of(19,4,5)
        );
    }


    @ParameterizedTest
    @MethodSource("provideArgumentsForProduce")
    void testProduce(int minesCount, int width, int height) {
        GameFactory factory = new MineCellGameFactory(minesCount, width, height);
        Game game = factory.produce();
        assertEquals(width * height, game.getCells().size());
        assertEquals(minesCount, (int) game.getCells().stream().filter(cell -> cell instanceof MineCell).count());
    }
}